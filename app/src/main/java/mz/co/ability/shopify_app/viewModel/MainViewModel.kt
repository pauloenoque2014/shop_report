package mz.co.ability.shopify_app.viewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import mz.co.ability.shopify_app.dataSource.entity.Customer
import mz.co.ability.shopify_app.model.beans.CustomerReportFeeder
import mz.co.ability.shopify_app.model.beans.LineItemReportFeeder
import mz.co.ability.shopify_app.repository.Repository

/**
 * Created by Paulo Enoque on 9/3/2017.
 */
class MainViewModel(application: Application): AndroidViewModel(application){

    private var repository: Repository
//    private var customerReport: CustomerReport

    init {
        repository = Repository(application)
//        customerReport = CustomerReport()
    }

    fun getCustomerReport(): LiveData<CustomerReportFeeder>?{
        repository.sync()
        val customer = Customer()
        customer.id = 4953626051.toLong()
        val feed = repository.getCustomerFeeds(customer)
        return feed
    }

    fun getItemReport(title: String): LiveData<LineItemReportFeeder>?{
        repository.sync()
        val feed = repository.getItemFeeds(title)
        return feed
    }

    fun sync() = repository.sync()

    fun getSyncDate() = repository.getSyncDate()

}