package mz.co.ability.shopify_app.dataSource.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import mz.co.ability.shopify_app.dataSource.entity.SyncEntity

/**
 * Created by Paulo Enoque on 9/9/2017.
 */
@Dao
interface SyncDAO{

    @Query("Select * from sync")
    fun getSyncDate(): LiveData<SyncEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(syncEntity: SyncEntity)

    @Delete
    fun delete(syncEntity: SyncEntity)
}