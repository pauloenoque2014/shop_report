package mz.co.ability.shopify_app.model.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LineItem {

    @SerializedName("id")
    @Expose
    var id: Long? = null
    @SerializedName("variant_id")
    @Expose
    var variantId: Long? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("quantity")
    @Expose
    var quantity: Long? = null
    @SerializedName("price")
    @Expose
    var price: String? = null
    @SerializedName("grams")
    @Expose
    var grams: Long? = null
    @SerializedName("sku")
    @Expose
    var sku: String? = null
    @SerializedName("variant_title")
    @Expose
    var variantTitle: String? = null
    @SerializedName("vendor")
    @Expose
    var vendor: String? = null
    @SerializedName("fulfillment_service")
    @Expose
    var fulfillmentService: String? = null
    @SerializedName("product_id")
    @Expose
    var productId: Long? = null
    @SerializedName("requires_shipping")
    @Expose
    var requiresShipping: Boolean? = null
    @SerializedName("taxable")
    @Expose
    var taxable: Boolean? = null
    @SerializedName("gift_card")
    @Expose
    var giftCard: Boolean? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("variant_inventory_management")
    @Expose
    var variantInventoryManagement: Any? = null
    @SerializedName("properties")
    @Expose
    var properties: List<Any>? = null
    @SerializedName("product_exists")
    @Expose
    var productExists: Boolean? = null
    @SerializedName("fulfillable_quantity")
    @Expose
    var fulfillableQuantity: Long? = null
    @SerializedName("total_discount")
    @Expose
    var totalDiscount: String? = null
    @SerializedName("fulfillment_status")
    @Expose
    var fulfillmentStatus: Any? = null
    @SerializedName("tax_lines")
    @Expose
    var taxLines: List<Any>? = null

}
