package mz.co.ability.shopify_app.dataSource.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Paulo Enoque on 8/28/2017.
 */
@Entity(foreignKeys = arrayOf(ForeignKey(entity = Order::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("order_id"))),
        tableName = "lineitem")
class LineItem{

    @PrimaryKey
    var id = 0.toLong()

    lateinit var title: String

    var quantity = 0.toLong()

    var fullfilable_quant: Long = 0.toLong()

    var order_id  =  0.toLong()

}