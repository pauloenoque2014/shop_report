package mz.co.ability.shopify_app.dataSource.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import mz.co.ability.shopify_app.dataSource.dao.CustomerDAO
import mz.co.ability.shopify_app.dataSource.dao.LineItemDAO
import mz.co.ability.shopify_app.dataSource.dao.OrderDAO
import mz.co.ability.shopify_app.dataSource.dao.SyncDAO
import mz.co.ability.shopify_app.dataSource.entity.Customer
import mz.co.ability.shopify_app.dataSource.entity.LineItem
import mz.co.ability.shopify_app.dataSource.entity.Order
import mz.co.ability.shopify_app.dataSource.entity.SyncEntity

/**
 * Created by Paulo Enoque on 8/28/2017.
 */
@Database(entities = arrayOf(Order::class,
                            Customer::class,
                            LineItem::class,
                            SyncEntity::class), version = 8)
abstract class AppDatabase: RoomDatabase() {
    abstract fun customerDAO(): CustomerDAO
    abstract fun orderDAO(): OrderDAO
    abstract fun lineItemDAO(): LineItemDAO
    abstract fun syncDAO(): SyncDAO

    //Using  SingleTon Pattern
    companion object {
        private var INSTANCE: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase?{
            if (INSTANCE == null){
                INSTANCE = Room.databaseBuilder(context.applicationContext,
                        AppDatabase::class.java, "shopfy-database")
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build()
            }
            return INSTANCE
        }

        fun destroyInstance(){
            INSTANCE = null
        }
    }
}