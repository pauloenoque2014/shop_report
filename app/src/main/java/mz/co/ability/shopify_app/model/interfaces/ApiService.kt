package mz.co.ability.shopify_app.model.interfaces

import mz.co.ability.shopify_app.model.retrofit.Order
import mz.co.ability.shopify_app.model.retrofit.Orders
import retrofit2.Call
import retrofit2.http.GET



/**
 * Created by Paulo Enoque on 9/3/2017.
 */
interface ApiService {
        /*
    Retrofit get annotation with our URL
    And our method that will return us the List of ContactList
    */
        @get:GET("/admin/orders.json?page=1&access_token=c32313df0d0ef512ca64d5b336a0d7c6")
        val myJSON: Call<Orders>

}