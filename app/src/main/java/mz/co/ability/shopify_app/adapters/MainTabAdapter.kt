package mz.co.ability.shopify_app.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import mz.co.ability.shopify_app.view.fragments.CustomerReportFragment
import mz.co.ability.shopify_app.view.fragments.ProductReportFragment

/**
 * Created by Paulo Enoque on 8/27/2017.
 */

class MainTabAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    private val titles = arrayOf("Customer", "Product")
    private lateinit var fragment: Fragment
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> fragment = CustomerReportFragment()
            1 -> fragment = ProductReportFragment()
            else -> fragment = Fragment()
        }
        return fragment
    }
    override fun getCount() = titles.size
    override fun getPageTitle(position: Int) = titles[position]
}