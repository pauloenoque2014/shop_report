package mz.co.ability.shopify_app.model.beans

import mz.co.ability.shopify_app.model.interfaces.ReportFeeder

/**
 * Created by Paulo Enoque on 8/26/2017.
 */
class CustomerReportFeeder: ReportFeeder{
    var id = 0

    lateinit var firstName: String

    lateinit var lastName: String

    lateinit var country: String

    lateinit var phone: String

    var totalSpent: Double = 0.0
}