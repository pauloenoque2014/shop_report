package mz.co.ability.shopify_app.dataSource.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import mz.co.ability.shopify_app.dataSource.entity.Customer
import mz.co.ability.shopify_app.dataSource.entity.Order
import mz.co.ability.shopify_app.model.beans.CustomerReportFeeder

/**
 * Created by Paulo Enoque on 8/28/2017.
 */
@Dao
interface OrderDAO{


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(order: Order)


}