package mz.co.ability.shopify_app.repository

import android.arch.lifecycle.LiveData
import android.content.Context
import mz.co.ability.shopify_app.dataSource.database.AppDatabase
import android.support.design.widget.Snackbar
import android.util.Log
import android.widget.Toast
import mz.co.ability.shopify_app.dataSource.dao.LineItemDAO
import mz.co.ability.shopify_app.view.activities.MainActivity
import mz.co.ability.shopify_app.dataSource.dao.RetroClient
import mz.co.ability.shopify_app.dataSource.entity.Customer
import mz.co.ability.shopify_app.dataSource.entity.LineItem
import mz.co.ability.shopify_app.dataSource.entity.SyncEntity
import mz.co.ability.shopify_app.model.beans.CustomerReportFeeder
import mz.co.ability.shopify_app.model.interfaces.ApiService
import mz.co.ability.shopify_app.model.retrofit.Order
import mz.co.ability.shopify_app.model.retrofit.Orders
import retrofit2.Call
import retrofit2.Response
import java.util.*
import javax.security.auth.callback.Callback


/**
 * Created by Paulo Enoque on 8/28/2017.
 */
class Repository(context: Context) {

    private var db: AppDatabase? = AppDatabase.getInstance(context)

    fun sync(): Boolean {
        //Creating an object of our api interface
        val api = RetroClient.getApiService()
        var syncStatus = false
        /**
         * Calling JSON
         */
        val call = api.myJSON

        /**
         * Enqueue Callback will be call when get response...
         */

        call.enqueue(object : retrofit2.Callback<Orders?> {
            override fun onFailure(call: Call<Orders?>?, t: Throwable?) {
                Log.d("erro", t.toString())
                syncStatus = false

            }

            override fun onResponse(call: Call<Orders?>?, response: Response<Orders?>?) {
                val customer = Customer()
                val order = mz.co.ability.shopify_app.dataSource.entity.Order()
                val items = mutableListOf<LineItem>()
                val lineitem = LineItem()
                val date = Date().toString()
                val sync = SyncEntity()

                sync.date = date
                sync.id = 1

                Log.d("Sucesso", response?.toString())

                for (i in response?.body()?.getOrders() ?: mutableListOf()) {
                    if (i.customer != null) {
                        customer.id = i.customer?.id ?: 0
                        customer.country = i.shippingAddress?.country ?: ""
                        customer.firstName = i.customer?.firstName ?: ""
                        customer.lastName = i.customer?.lastName ?: ""
                        customer.phone = i.customer?.phone as String? ?: ""

                        order.customer_id = i.customer?.id ?: 0

                    }


                    order.id = i?.id ?: 0
                    order.confirmed = i.confirmed ?: false
                    order.currency = i.currency ?: ""
                    order.customer_id = i.customer?.id ?: 0
                    order.financial_status = i.financialStatus ?: ""
                    order.total_spent = i.totalPrice?.toDouble() ?: 0.0
                    order.gateway = i.gateway.toString()
                    if (i.customer == null)
                        order.customer_id = null
                    Log.d("customer", order.customer_id.toString())

                    for (item in i.lineItems ?: listOf()) {
                        items.clear()
                        lineitem.id = item?.id ?: 0
                        lineitem.title = item?.title ?: ""
                        lineitem.fullfilable_quant = item?.fulfillableQuantity ?: 0
                        lineitem.quantity = item?.quantity ?: 0
                        lineitem.order_id = i?.id ?: 0

                        items.add(lineitem)

                    }
                    db?.customerDAO()?.insert(customer)
                    db?.orderDAO()?.insert(order)
                    db?.lineItemDAO()?.insertAll(items)

                }
                    syncStatus = true
                    db?.syncDAO()?.insert(sync)
            }
        })


        return syncStatus
    }

    fun getAllLineItems() = db?.lineItemDAO()?.getAllItems()

    fun getSyncDate() = db?.syncDAO()?.getSyncDate()

    fun getCustomerFeeds(customer: Customer) = db?.customerDAO()?.getFeeder(customer.id)

    fun getItemFeeds(title: String) = db?.lineItemDAO()?.getLineItemFromTitle(title)
}