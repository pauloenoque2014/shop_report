package mz.co.ability.shopify_app.model.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Paulo Enoque on 9/4/2017.
 */
class Orders{
    @SerializedName("orders")
    @Expose
    private var orders = mutableListOf<Order>()
    fun getOrders() = orders
}