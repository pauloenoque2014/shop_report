package mz.co.ability.shopify_app.model.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Customer {

    @SerializedName("id")
    @Expose
    var id: Long? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("accepts_marketing")
    @Expose
    var acceptsMarketing: Boolean? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("first_name")
    @Expose
    var firstName: String? = null
    @SerializedName("last_name")
    @Expose
    var lastName: String? = null
    @SerializedName("orders_count")
    @Expose
    var ordersCount: Long? = null
    @SerializedName("state")
    @Expose
    var state: String? = null
    @SerializedName("total_spent")
    @Expose
    var totalSpent: String? = null
    @SerializedName("last_order_id")
    @Expose
    var lastOrderId: Long? = null
    @SerializedName("note")
    @Expose
    var note: Any? = null
    @SerializedName("verified_email")
    @Expose
    var verifiedEmail: Boolean? = null
    @SerializedName("multipass_identifier")
    @Expose
    var multipassIdentifier: Any? = null
    @SerializedName("tax_exempt")
    @Expose
    var taxExempt: Boolean? = null
    @SerializedName("phone")
    @Expose
    var phone: Any? = null
    @SerializedName("tags")
    @Expose
    var tags: String? = null
    @SerializedName("last_order_name")
    @Expose
    var lastOrderName: String? = null
    @SerializedName("default_address")
    @Expose
    var defaultAddress: DefaultAddress? = null

}
