package mz.co.ability.shopify_app.model.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ShippingAddress {

    @SerializedName("first_name")
    @Expose
    var firstName: String? = null
    @SerializedName("address1")
    @Expose
    var address1: String? = null
    @SerializedName("phone")
    @Expose
    var phone: String? = null
    @SerializedName("city")
    @Expose
    var city: String? = null
    @SerializedName("zip")
    @Expose
    var zip: String? = null
    @SerializedName("province")
    @Expose
    var province: String? = null
    @SerializedName("country")
    @Expose
    var country: String? = null
    @SerializedName("last_name")
    @Expose
    var lastName: String? = null
    @SerializedName("address2")
    @Expose
    var address2: Any? = null
    @SerializedName("company")
    @Expose
    var company: Any? = null
    @SerializedName("latitude")
    @Expose
    var latitude: Double? = null
    @SerializedName("longitude")
    @Expose
    var longitude: Double? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("country_code")
    @Expose
    var countryCode: String? = null
    @SerializedName("province_code")
    @Expose
    var provinceCode: String? = null

}
