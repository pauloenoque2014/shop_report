package mz.co.ability.shopify_app.dataSource.dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mz.co.ability.shopify_app.model.interfaces.ApiService;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Paulo Enoque on 9/3/2017.
 */

public class RetroClient {
    /********
     * URLS
     *******/
    private static final String ROOT_URL = "https://shopicruit.myshopify.com";
    private static final Gson gson = new GsonBuilder()
                                            .setDateFormat("yyyy-mm-dd'T'HH:mm:ssZ")
                                            .create();

    /**
     * Get Retrofit Instance
     */
    private static Retrofit getRetrofitInstance() {

        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static ApiService getApiService() {
        return getRetrofitInstance().create(ApiService.class);
    }
}
