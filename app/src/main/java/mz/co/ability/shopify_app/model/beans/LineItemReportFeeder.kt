package mz.co.ability.shopify_app.model.beans

import mz.co.ability.shopify_app.model.interfaces.ReportFeeder

/**
 * Created by Paulo Enoque on 8/26/2017.
 */
class LineItemReportFeeder : ReportFeeder{
    var id = 0.toLong()

    lateinit var title: String

     var quantity: Long = 0

     var fullfilable_quant: Long = 0

}