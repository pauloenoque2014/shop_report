package mz.co.ability.shopify_app.model.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Order {

    @SerializedName("id")
    @Expose
    var id: Long? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("closed_at")
    @Expose
    var closedAt: Any? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("number")
    @Expose
    var number: Long? = null
    @SerializedName("note")
    @Expose
    var note: Any? = null
    @SerializedName("token")
    @Expose
    var token: String? = null
    @SerializedName("gateway")
    @Expose
    var gateway: String? = null
    @SerializedName("test")
    @Expose
    var test: Boolean? = null
    @SerializedName("total_price")
    @Expose
    var totalPrice: String? = null
    @SerializedName("subtotal_price")
    @Expose
    var subtotalPrice: String? = null
    @SerializedName("total_weight")
    @Expose
    var totalWeight: Long? = null
    @SerializedName("total_tax")
    @Expose
    var totalTax: String? = null
    @SerializedName("taxes_included")
    @Expose
    var taxesIncluded: Boolean? = null
    @SerializedName("currency")
    @Expose
    var currency: String? = null
    @SerializedName("financial_status")
    @Expose
    var financialStatus: String? = null
    @SerializedName("confirmed")
    @Expose
    var confirmed: Boolean? = null
    @SerializedName("total_discounts")
    @Expose
    var totalDiscounts: String? = null
    @SerializedName("total_line_items_price")
    @Expose
    var totalLineItemsPrice: String? = null
    @SerializedName("cart_token")
    @Expose
    var cartToken: Any? = null
    @SerializedName("buyer_accepts_marketing")
    @Expose
    var buyerAcceptsMarketing: Boolean? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("referring_site")
    @Expose
    var referringSite: Any? = null
    @SerializedName("landing_site")
    @Expose
    var landingSite: Any? = null
    @SerializedName("cancelled_at")
    @Expose
    var cancelledAt: Any? = null
    @SerializedName("cancel_reason")
    @Expose
    var cancelReason: Any? = null
    @SerializedName("total_price_usd")
    @Expose
    var totalPriceUsd: String? = null
    @SerializedName("checkout_token")
    @Expose
    var checkoutToken: Any? = null
    @SerializedName("reference")
    @Expose
    var reference: Any? = null
    @SerializedName("user_id")
    @Expose
    var userId: Long? = null
    @SerializedName("location_id")
    @Expose
    var locationId: Any? = null
    @SerializedName("source_identifier")
    @Expose
    var sourceIdentifier: Any? = null
    @SerializedName("source_url")
    @Expose
    var sourceUrl: Any? = null
    @SerializedName("processed_at")
    @Expose
    var processedAt: String? = null
    @SerializedName("device_id")
    @Expose
    var deviceId: Any? = null
    @SerializedName("phone")
    @Expose
    var phone: Any? = null
    @SerializedName("customer_locale")
    @Expose
    var customerLocale: Any? = null
    @SerializedName("browser_ip")
    @Expose
    var browserIp: Any? = null
    @SerializedName("landing_site_ref")
    @Expose
    var landingSiteRef: Any? = null
    @SerializedName("order_number")
    @Expose
    var orderNumber: Long? = null
    @SerializedName("discount_codes")
    @Expose
    var discountCodes: List<Any>? = null
    @SerializedName("note_attributes")
    @Expose
    var noteAttributes: List<Any>? = null
    @SerializedName("payment_gateway_names")
    @Expose
    var paymentGatewayNames: List<String>? = null
    @SerializedName("processing_method")
    @Expose
    var processingMethod: String? = null
    @SerializedName("checkout_id")
    @Expose
    var checkoutId: Any? = null
    @SerializedName("source_name")
    @Expose
    var sourceName: String? = null
    @SerializedName("fulfillment_status")
    @Expose
    var fulfillmentStatus: Any? = null
    @SerializedName("tax_lines")
    @Expose
    var taxLines: List<Any>? = null
    @SerializedName("tags")
    @Expose
    var tags: String? = null
    @SerializedName("contact_email")
    @Expose
    var contactEmail: String? = null
    @SerializedName("order_status_url")
    @Expose
    var orderStatusUrl: Any? = null
    @SerializedName("line_items")
    @Expose
    var lineItems: List<LineItem>? = null
    @SerializedName("shipping_lines")
    @Expose
    var shippingLines: List<Any>? = null
    @SerializedName("billing_address")
    @Expose
    var billingAddress: BillingAddress? = null
    @SerializedName("shipping_address")
    @Expose
    var shippingAddress: ShippingAddress? = null
    @SerializedName("fulfillments")
    @Expose
    var fulfillments: List<Any>? = null
    @SerializedName("refunds")
    @Expose
    var refunds: List<Any>? = null
    @SerializedName("customer")
    @Expose
    var customer: Customer? = null

}
