package mz.co.ability.shopify_app.dataSource.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Paulo Enoque on 8/28/2017.
 */
@Entity(foreignKeys = arrayOf(ForeignKey(entity = Customer::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("customer_id"))),
        tableName = "orders")
class Order{

    @PrimaryKey
    var id = 0.toLong()

    lateinit var currency: String

    lateinit var financial_status: String

    var gateway = ""

    var confirmed = false

    var total_spent = 0.0

    var customer_id: Long? = null
}