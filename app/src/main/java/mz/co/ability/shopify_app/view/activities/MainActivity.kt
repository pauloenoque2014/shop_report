package mz.co.ability.shopify_app.view.activities

import android.arch.lifecycle.LifecycleActivity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_customer_report.*
import kotlinx.android.synthetic.main.fragment_product_report.*
import mz.co.ability.shopify_app.R
import mz.co.ability.shopify_app.adapters.MainTabAdapter
import mz.co.ability.shopify_app.dataSource.entity.SyncEntity
import mz.co.ability.shopify_app.model.beans.CustomerReportFeeder
import mz.co.ability.shopify_app.model.beans.LineItemReportFeeder
import mz.co.ability.shopify_app.viewModel.MainViewModel

class MainActivity : LifecycleActivity() {
    private lateinit var adapter: MainTabAdapter
    private lateinit var viewmodel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = MainTabAdapter(supportFragmentManager)

        pager.adapter = adapter
        main_tab.setupWithViewPager(pager)

        viewmodel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewmodel.getCustomerReport()?.observe(this, Observer<CustomerReportFeeder?> { t ->
            if (t?.firstName != null){
                initName.text = t?.firstName.first().toString() + t?.lastName.first()
                fullname.text = t?.firstName + " " + t?.lastName
                totalSpent.text = "Total Spent: " +t?.totalSpent.toString()
                currency.text = "Country: " +t?.country
            }
        })

        viewmodel.getItemReport("Awesome Bronze Bag")?.observe(this, Observer<LineItemReportFeeder?> {
            if (it != null){
                initTitle.text =  it.title.first().toString()
                itemTitle.text = it.title
                quantity.text = "Quatity: " + it.fullfilable_quant.toString()
            }
            Log.d("bags", it.toString())
        })

        viewmodel.getSyncDate()?.observe(this, Observer<SyncEntity?> {
            syncText.text = "Last Syncronized at: " + it?.date
        })

        syncImage.setOnClickListener(View.OnClickListener {
            viewmodel.sync()
        })

    }
}
