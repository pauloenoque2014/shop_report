package mz.co.ability.shopify_app.dataSource.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Paulo Enoque on 9/9/2017.
 */
@Entity(tableName = "sync")
class SyncEntity{

    @PrimaryKey
    var id = 0.toLong()

    var date = ""
}