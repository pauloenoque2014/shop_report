package mz.co.ability.shopify_app.dataSource.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Paulo Enoque on 8/26/2017.
 */
@Entity
class Customer{

    @PrimaryKey
    //0 is the default value
    var  id = 0.toLong()

    lateinit var firstName: String

    lateinit var lastName: String

    lateinit var country: String

     var phone: String? = ""
}