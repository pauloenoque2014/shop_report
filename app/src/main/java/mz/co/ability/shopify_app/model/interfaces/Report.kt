package mz.co.ability.shopify_app.model.interfaces

import android.arch.lifecycle.LiveData

/**
 * Created by Paulo Enoque on 8/26/2017.
 */
interface Report{
    fun build() : LiveData<ReportFeeder>
}