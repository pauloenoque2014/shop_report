package mz.co.ability.shopify_app.dataSource.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import mz.co.ability.shopify_app.dataSource.entity.LineItem
import mz.co.ability.shopify_app.model.beans.CustomerReportFeeder
import mz.co.ability.shopify_app.model.beans.LineItemReportFeeder

/**
 * Created by Paulo Enoque on 9/7/2017.
 */
@Dao
interface LineItemDAO{

    @Query("select lineitem.id , title, sum(quantity) as quantity, sum(fullfilable_quant) as fullfilable_quant from lineitem  join orders  on order_id = orders.id where title LIKE :arg0 AND financial_status LIKE 'paid' group by title")
    fun getLineItemFromTitle(title: String): LiveData<LineItemReportFeeder>

    @Query("Select * from lineitem")
    fun getAllItems(): List<LineItemReportFeeder>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(items: List<LineItem>)
}