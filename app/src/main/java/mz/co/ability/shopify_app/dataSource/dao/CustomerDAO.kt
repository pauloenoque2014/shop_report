package mz.co.ability.shopify_app.dataSource.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import mz.co.ability.shopify_app.dataSource.entity.Customer
import mz.co.ability.shopify_app.model.beans.CustomerReportFeeder

/**
 * Created by Paulo Enoque on 8/28/2017.
 */
@Dao
interface CustomerDAO{

    @Query("select customer.id , firstName, lastName, country, phone, totalSpent from customer join (select orders.id, sum(total_spent) as totalSpent, customer_id from orders where currency LIKE 'CAD' group by customer_id) on customer.id = customer_id")
    fun getAllFeeder(): List<CustomerReportFeeder>

    @Query("select customer.id , firstName, lastName, country, phone, totalSpent from customer join (select orders.id, sum(total_spent) as totalSpent, customer_id from orders where currency LIKE 'CAD' and gateway NOT LIKE '' group by customer_id) on customer.id = customer_id where customer_id = :arg0")
    fun getFeeder(customeIdr: Long): LiveData<CustomerReportFeeder>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(customers: List<Customer>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(customers: Customer)

    @Delete
    fun deleteAll(vararg customers: Customer)
}