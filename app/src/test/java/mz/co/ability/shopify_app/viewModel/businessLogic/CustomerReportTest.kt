package mz.co.ability.shopify_app.viewModel.businessLogic

import mz.co.ability.shopify_app.dataSource.entity.Customer
import mz.co.ability.shopify_app.model.beans.CustomerReportFeeder
import org.junit.After
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mockito

/**
 * Created by Paulo Enoque on 8/26/2017.
 */
class CustomerReportTest {
    lateinit var customers: List<CustomerReportFeeder>

    @Before
    fun setUp() {
        var customer1 = CustomerReportFeeder()
        var customer2 = CustomerReportFeeder()
        var customer3 = CustomerReportFeeder()

        //Customer 1 set up
        customer1.id = 1
        customer1.country = "United States"
        customer1.firstName = "Napoleon"
        customer1.lastName = "Batz"
        customer1.phone = "+55"
        customer1.totalSpent =  1000.0

        //Customer 2 set up
        customer2.id = 2
        customer2.country = "United States"
        customer2.firstName = "Paul"
        customer2.lastName = "Batz"
        customer2.phone = "+55"
        customer2.totalSpent =  2000.0

        //Customer 3 set up
        customer3.id = 3
        customer3.country = "United States"
        customer3.firstName = "Santos"
        customer3.lastName = "Batz"
        customer3.phone = "+55"
        customer3.totalSpent =  3000.0
        customers  = listOf<CustomerReportFeeder>(
                customer1,
                customer2,
                customer3
        )
    }

    @After
    fun tearDown() {
    }

    @Test
    fun checkDatafromReportFactory(){
        val my_customer = Mockito.mock(Customer::class.java)
        val customerReport = CustomerReport()
        my_customer.id =1
        my_customer.firstName = "Napoleon"
        my_customer.lastName = "Batz"
        my_customer.country = "United States"


        val feeder  = customerReport.reportFactory(my_customer, customers).build()
        assertEquals(customers[0], feeder)
    }

}